<?php

namespace App\Repository;

use App\Entity\ShoppingListArticlesQuantity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ShoppingListArticlesQuantity>
 *
 * @method ShoppingListArticlesQuantity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShoppingListArticlesQuantity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShoppingListArticlesQuantity[]    findAll()
 * @method ShoppingListArticlesQuantity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShoppingListArticlesQuantityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShoppingListArticlesQuantity::class);
    }

    public function save(ShoppingListArticlesQuantity $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ShoppingListArticlesQuantity $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param ShoppingList The shopping list that own articles
     * @return ShoppingListArticlesQuantity[] Returns an array of ShoppingListArticlesQuantity objects
     */
    public function findShoppingListArticles($shoppingList): array
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.shoppingListId = :val')
            ->setParameter('val', $shoppingList)
            ->orderBy('s.store', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param ShoppingList The shopping list that own articles
     * @return int The number of articles in the list
     */
    public function findNumberOfArticles($shoppingListId): int
    {
        return $this->getEntityManager()->getConnection()->prepare(
            '
            SELECT COALESCE(SUM(quantity), 0)
            FROM shopping_list_articles_quantity
            WHERE shopping_list_id_id = :listId;
            '
        )->executeQuery(["listId" => $shoppingListId])->fetchOne();
    }

    /**
     * @param ShoppingList The shopping list that own articles
     * @return int The price of the sum of all articles
     */
    public function findTotalPrice($shoppingList): float|null
    {
        return $this->getEntityManager()->getConnection()->prepare(
            'SELECT SUM(s.quantity * p.price)
            FROM shopping_list_articles_quantity as s
            JOIN article_pricing as p ON s.article_id_id = p.article_id_id AND s.store_id = p.store_id_id
            WHERE s.shopping_list_id_id = :listId;'
        )->executeQuery(["listId" => $shoppingList->getId()])->fetchOne();
    }

    /**
     * @param ShoppingList The shopping list that own articles
     * @return int The AVG, MAX, MIN of articles grouped by stores
     */
    public function findStatsPerStore($shoppingList, $store): array|null
    {
        return $this->getEntityManager()->getConnection()->prepare(
            'SELECT SUM(s.quantity * p.price) as total,
                AVG(p.price) as avg,
                MAX(p.price) as max,
                MIN(p.price) as min
            FROM shopping_list_articles_quantity as s
            JOIN article_pricing as p ON s.article_id_id = p.article_id_id AND s.store_id = p.store_id_id
            WHERE s.shopping_list_id_id = :listId and p.store_id_id = :storeId
            GROUP BY p.store_id_id;'
        )->executeQuery(["listId" => $shoppingList->getId(), "storeId" => $store->getId()])->fetchAllAssociative()[0];
    }

//    /**
//     * @return ShoppingListArticlesQuantity[] Returns an array of ShoppingListArticlesQuantity objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ShoppingListArticlesQuantity
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

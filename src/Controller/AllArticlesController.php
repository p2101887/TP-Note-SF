<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\SecurityBundle\Security;
use App\Repository\ShoppingListArticlesQuantityRepository;

class AllArticlesController extends AbstractController
{
    #[Route('/all-articles', name: 'app_all_articles')]
    public function index(Security $security, ArticleRepository $articleRepository, ShoppingListArticlesQuantityRepository $quantity): Response
    {

        $articles = $articleRepository->findAll();
        foreach($articles as $article) {
            foreach($article->getArticlePricings()->getValues() as $pricing) {
                $price = $pricing->getPrice();
                dump($pricing);
            }
            
        }

        return $this->render('all_articles/index.html.twig', [
            'controller_name' => 'AllArticlesController',
            'user' => $security->getUser(),
            'isAdmin' => in_Array('ROLE_ADMIN',$security->getUser()->getRoles()),
            'articles' => $articleRepository->findAll(),
            'pricing' => $price,
        ]);
    }
}

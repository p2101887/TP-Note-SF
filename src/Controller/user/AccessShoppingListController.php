<?php

namespace App\Controller\user;

use App\Entity\AccessShoppingList;
use App\Form\AccessShoppingListType;
use App\Repository\AccessShoppingListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/accessList')]
class AccessShoppingListController extends AbstractController
{
    #[Route('/', name: 'app_access_shopping_list_index', methods: ['GET'])]
    public function index(AccessShoppingListRepository $accessShoppingListRepository): Response
    {
        return $this->render('access_shopping_list/index.html.twig', [
            'access_shopping_lists' => $accessShoppingListRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_access_shopping_list_new', methods: ['GET', 'POST'])]
    public function new(Request $request, AccessShoppingListRepository $accessShoppingListRepository): Response
    {
        $accessShoppingList = new AccessShoppingList();
        $form = $this->createForm(AccessShoppingListType::class, $accessShoppingList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $accessShoppingListRepository->save($accessShoppingList, true);

            return $this->redirectToRoute('app_access_shopping_list_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('access_shopping_list/new.html.twig', [
            'access_shopping_list' => $accessShoppingList,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_access_shopping_list_show', methods: ['GET'])]
    public function show(AccessShoppingList $accessShoppingList): Response
    {
        return $this->render('access_shopping_list/show.html.twig', [
            'access_shopping_list' => $accessShoppingList,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_access_shopping_list_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, AccessShoppingList $accessShoppingList, AccessShoppingListRepository $accessShoppingListRepository): Response
    {
        $form = $this->createForm(AccessShoppingListType::class, $accessShoppingList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $accessShoppingListRepository->save($accessShoppingList, true);

            return $this->redirectToRoute('app_access_shopping_list_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('access_shopping_list/edit.html.twig', [
            'access_shopping_list' => $accessShoppingList,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_access_shopping_list_delete', methods: ['POST'])]
    public function delete(Request $request, AccessShoppingList $accessShoppingList, AccessShoppingListRepository $accessShoppingListRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$accessShoppingList->getId(), $request->request->get('_token'))) {
            $accessShoppingListRepository->remove($accessShoppingList, true);
        }

        return $this->redirectToRoute('app_shopping_list_access', ["id" => $accessShoppingList->getShoppingListId()->getId()], Response::HTTP_SEE_OTHER);
    }
}

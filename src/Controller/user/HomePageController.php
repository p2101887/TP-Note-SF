<?php

namespace App\Controller\user;

use App\Repository\AccessShoppingListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\SecurityBundle\Security;

class HomePageController extends AbstractController
{
    #[Route('/home', name: 'app_home_page')]
    public function index(Security $security, AccessShoppingListRepository $accessShoppingListRepository): Response
    {
        $shoppingLists = $accessShoppingListRepository->findUserShoppingLists(
            $security->getUser()
        );

        return $this->render('home_page/index.html.twig', [
            'controller_name' => 'HomePageController',
            'access_shopping_lists' => $shoppingLists,
            'user' => $security->getUser(),
            'isAdmin' => in_Array('ROLE_ADMIN',$security->getUser()->getRoles()),
        ]);
    }
}

<?php

namespace App\Controller\user;

use App\Entity\ShoppingList;
use App\Form\ShoppingListType;
use App\Repository\ShoppingListRepository;
use App\Repository\UserRepository;
use App\Entity\AccessShoppingList;
use App\Entity\Article;
use App\Entity\ShoppingListArticlesQuantity;
use App\Form\CreateAccessFormType;
use App\Form\ShoppingListArticlesQuantityType;
use App\Repository\AccessShoppingListRepository;
use App\Repository\ShoppingListArticlesQuantityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\SecurityBundle\Security;

#[Route('/shoppingList')]
class ShoppingListController extends AbstractController
{
    #[Route('/', name: 'app_shopping_list_index', methods: ['GET'])]
    public function index(AccessShoppingListRepository $accessShoppingListRepository, Security $security): Response
    {
        $this->redirectToRoute("app_home_page");

        return $this->render('shopping_list/index.html.twig', [
            'shopping_lists' => $accessShoppingListRepository->findUserShoppingLists($security->getUser()),
            'isAdmin' => in_Array('ROLE_ADMIN',$security->getUser()->getRoles()),
        ]);
    }

    #[Route('/new', name: 'app_shopping_list_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ShoppingListRepository $shoppingListRepository, UserRepository $userRepository, AccessShoppingListRepository $accessShoppingList, Security $security): Response
    {
        $shoppingList = new ShoppingList();
        $form = $this->createForm(ShoppingListType::class, $shoppingList);
        $form->handleRequest($request);
        
        /** @var User */
        $currentUser = $this->getUser();
        $userId = $currentUser->getId();
        $arrayListOfArticles = null;

        $arrayListofShoppingList = $userRepository->findOneById($userId)->getShoppingLists()->getValues();
        foreach ($arrayListofShoppingList as $list) {
            $arrayListOfArticles = $list->getShoppingListId()->getArticlesQuantity()->getValues();
            foreach ($arrayListOfArticles as $article) {
                if (!$article->isPurshased()){
                    $shoppingList->addArticlesQuantity($article);
                }
            }
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $shoppingListRepository->save($shoppingList, true);

            $access = new AccessShoppingList();
            $access->setUserId($security->getUser());
            $access->setPermission("OWNER");
            $access->setShoppingListId($shoppingList);

            $accessShoppingList->save($access, true);

            return $this->redirectToRoute('app_shopping_list_show', ["id" => $shoppingList->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('shopping_list/new.html.twig', [
            'shopping_list' => $shoppingList,
            'form' => $form,
            'isAdmin' => in_Array('ROLE_ADMIN',$security->getUser()->getRoles()),
        ]);
    }

    #[Route('/{id}', name: 'app_shopping_list_show', methods: ['GET', 'POST'])]
    public function show(Request $request, ShoppingList $shoppingList, ShoppingListArticlesQuantityRepository $quantity, Security $security): Response
    {
        $addArticle = (new ShoppingListArticlesQuantity())
            ->setShoppingListId($shoppingList);
        $addArticle->setPurshased(false);
        $addArticleForm = $this->createForm(ShoppingListArticlesQuantityType::class, $addArticle);
        $addArticleForm->handleRequest($request);

        if($addArticleForm->isSubmitted() && $addArticleForm->isValid()) {
            $quantity->save($addArticle, true);

            $this->redirectToRoute('app_shopping_list_show', ["id" => $shoppingList->getId()], Response::HTTP_SEE_OTHER);
        }

        $articlesQuantity = $quantity->findShoppingListArticles($shoppingList);
    
        $i = -1;
        $stores = [];
        foreach ($articlesQuantity as $tmp) {
            if (!in_array($tmp->getStore(), $stores)){
                $stores[$i++] = $tmp->getStore();
            }
        }
        sort($stores);

        $perStores = [];
        foreach($stores as $store) {
            $tmp = $quantity->findStatsPerStore($shoppingList, $store);
            $perStores[$store->getId()] = [
                'total' => $tmp['total'],
                'avg' => $tmp['avg'],
                'max' => $tmp['max'],
                'min' => $tmp['min']
            ];
        }

        return $this->render('shopping_list/show.html.twig', [
            'shopping_list' => $shoppingList,
            'articles_quantity' => $articlesQuantity,
            'form' => $addArticleForm,
            'isAdmin' => in_Array('ROLE_ADMIN',$security->getUser()->getRoles()),
            'stores' => $stores,
            'stats' => [
                'numberOfArticles' => $quantity->findNumberOfArticles($shoppingList->getId()),
                'totalPrice' => $quantity->findTotalPrice($shoppingList),
                'perStores' => $perStores,
            ]
        ]);
    }

    #[Route('/{id}/access', name: 'app_shopping_list_access', methods: ['GET', 'POST'])]
    public function access(Request $request, Security $security, ShoppingList $shoppingList, AccessShoppingListRepository $accessShoppingListRepository)
    {
        $newAccess = new AccessShoppingList();
        $newAccess->setShoppingListId($shoppingList);
        $addAccessForm = $this->createForm(CreateAccessFormType::class, $newAccess);
        $addAccessForm->handleRequest($request);

        if ($addAccessForm->isSubmitted() && $addAccessForm->isValid()) {
            $accessShoppingListRepository->save($newAccess, true);
        }
        
        $accesses = $accessShoppingListRepository->findShoppingListAccesses($shoppingList);

        return $this->render('shopping_list/access.html.twig', [
            'isAdmin' => in_Array('ROLE_ADMIN',$security->getUser()->getRoles()),
            'shopping_list' => $shoppingList,
            'addAccessForm' => $addAccessForm,
            'accesses' => $accesses,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_shopping_list_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ShoppingList $shoppingList, ShoppingListRepository $shoppingListRepository, Security $security): Response
    {
        $form = $this->createForm(ShoppingListType::class, $shoppingList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shoppingListRepository->save($shoppingList, true);

            return $this->redirectToRoute('app_home_page', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('shopping_list/edit.html.twig', [
            'shopping_list' => $shoppingList,
            'form' => $form,
            'isAdmin' => in_Array('ROLE_ADMIN',$security->getUser()->getRoles()),
        ]);
    }

    #[Route('/{id}/delete', name: 'app_shopping_list_delete', methods: ['POST'])]
    public function delete(Request $request, ShoppingList $shoppingList, ShoppingListRepository $shoppingListRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$shoppingList->getId(), $request->request->get('_token'))) {
            $shoppingListRepository->remove($shoppingList, true);
        }

        return $this->redirectToRoute('app_home_page', [], Response::HTTP_SEE_OTHER);
    }
}

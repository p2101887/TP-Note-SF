<?php

namespace App\Controller\user;

use App\Entity\ShoppingListArticlesQuantity;
use App\Form\ShoppingListArticlesQuantityType;
use App\Repository\ShoppingListArticlesQuantityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/shoppingQuantity')]
class ShoppingListArticlesQuantityController extends AbstractController
{
    #[Route('/', name: 'app_shopping_list_articles_quantity_index', methods: ['GET'])]
    public function index(ShoppingListArticlesQuantityRepository $shoppingListArticlesQuantityRepository): Response
    {
        return $this->render('shopping_list_articles_quantity/index.html.twig', [
            'shopping_list_articles_quantities' => $shoppingListArticlesQuantityRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_shopping_list_articles_quantity_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ShoppingListArticlesQuantityRepository $shoppingListArticlesQuantityRepository): Response
    {
        $shoppingListArticlesQuantity = new ShoppingListArticlesQuantity();
        $form = $this->createForm(ShoppingListArticlesQuantityType::class, $shoppingListArticlesQuantity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shoppingListArticlesQuantityRepository->save($shoppingListArticlesQuantity, true);

            return $this->redirectToRoute('app_shopping_list_articles_quantity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('shopping_list_articles_quantity/new.html.twig', [
            'shopping_list_articles_quantity' => $shoppingListArticlesQuantity,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_shopping_list_articles_quantity_show', methods: ['GET'])]
    public function show(ShoppingListArticlesQuantity $shoppingListArticlesQuantity): Response
    {
        return $this->render('shopping_list_articles_quantity/show.html.twig', [
            'shopping_list_articles_quantity' => $shoppingListArticlesQuantity,
        ]);
    }

    #[Route('/{id}/quickedit/quantity', name: 'app_shopping_list_articles_quantity_quick_edit', methods: ['POST'])]
    public function quickEdit(Request $request, ShoppingListArticlesQuantity $shoppingListArticlesQuantity, ShoppingListArticlesQuantityRepository $shoppingListArticlesQuantityRepository): Response
    {
        if ($request->request->get('newQuantity') !== null) {
            $shoppingListArticlesQuantity->setQuantity($request->request->get('newQuantity'));

            $shoppingListArticlesQuantityRepository->save($shoppingListArticlesQuantity, true);
        }
        
        return $this->redirectToRoute('app_shopping_list_show', ["id" => $shoppingListArticlesQuantity->getShoppingListId()->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/quickedit/purshased', name: 'app_shopping_list_articles_purshased_quick_edit', methods: ['POST'])]
    public function quickEditPurshased(Request $request, ShoppingListArticlesQuantity $shoppingListArticlesQuantity, ShoppingListArticlesQuantityRepository $shoppingListArticlesQuantityRepository): Response
    {
        $checked = $request->request->get('newPurshased');
        if (!is_null($checked) && strcmp($checked, "on") == 0) {
            $shoppingListArticlesQuantity->setPurshased(true);
        } else {
            $shoppingListArticlesQuantity->setPurshased(false);
        }
        
        $shoppingListArticlesQuantityRepository->save($shoppingListArticlesQuantity, true);
        
        return $this->redirectToRoute('app_shopping_list_show', ["id" => $shoppingListArticlesQuantity->getShoppingListId()->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/edit', name: 'app_shopping_list_articles_quantity_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ShoppingListArticlesQuantity $shoppingListArticlesQuantity, ShoppingListArticlesQuantityRepository $shoppingListArticlesQuantityRepository): Response
    {
        $form = $this->createForm(ShoppingListArticlesQuantityType::class, $shoppingListArticlesQuantity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shoppingListArticlesQuantityRepository->save($shoppingListArticlesQuantity, true);

            return $this->redirectToRoute('app_shopping_list_articles_quantity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('shopping_list_articles_quantity/edit.html.twig', [
            'shopping_list_articles_quantity' => $shoppingListArticlesQuantity,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_shopping_list_articles_quantity_delete', methods: ['POST'])]
    public function delete(Request $request, ShoppingListArticlesQuantity $shoppingListArticlesQuantity, ShoppingListArticlesQuantityRepository $shoppingListArticlesQuantityRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$shoppingListArticlesQuantity->getId(), $request->request->get('_token'))) {
            $shoppingListArticlesQuantityRepository->remove($shoppingListArticlesQuantity, true);
        }

        return $this->redirectToRoute('app_shopping_list_show', ["id" => $shoppingListArticlesQuantity->getShoppingListId()->getId()], Response::HTTP_SEE_OTHER);
    }
}

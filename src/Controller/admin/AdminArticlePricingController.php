<?php

namespace App\Controller\admin;

use App\Entity\ArticlePricing;
use App\Form\ArticlePricingType;
use App\Repository\ArticlePricingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/pricing')]
class AdminArticlePricingController extends AbstractController
{
    #[Route('/', name: 'app_article_pricing_index', methods: ['GET'])]
    public function index(ArticlePricingRepository $articlePricingRepository): Response
    {
        return $this->render('admin/article_pricing/index.html.twig', [
            'article_pricings' => $articlePricingRepository->createQueryBuilder("ap")->orderBy("ap.articleId", "DESC")->getQuery()->execute(),
        ]);
    }

    #[Route('/new', name: 'app_article_pricing_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ArticlePricingRepository $articlePricingRepository): Response
    {
        $articlePricing = new ArticlePricing();
        $form = $this->createForm(ArticlePricingType::class, $articlePricing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articlePricingRepository->save($articlePricing, true);

            return $this->redirectToRoute('app_article_pricing_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin/article_pricing/new.html.twig', [
            'article_pricing' => $articlePricing,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_article_pricing_show', methods: ['GET'])]
    public function show(ArticlePricing $articlePricing): Response
    {
        return $this->render('admin/article_pricing/show.html.twig', [
            'article_pricing' => $articlePricing,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_article_pricing_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ArticlePricing $articlePricing, ArticlePricingRepository $articlePricingRepository): Response
    {
        $form = $this->createForm(ArticlePricingType::class, $articlePricing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articlePricingRepository->save($articlePricing, true);

            return $this->redirectToRoute('app_article_pricing_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin/article_pricing/edit.html.twig', [
            'article_pricing' => $articlePricing,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_article_pricing_delete', methods: ['POST'])]
    public function delete(Request $request, ArticlePricing $articlePricing, ArticlePricingRepository $articlePricingRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$articlePricing->getId(), $request->request->get('_token'))) {
            $articlePricingRepository->remove($articlePricing, true);
        }

        return $this->redirectToRoute('app_article_pricing_index', [], Response::HTTP_SEE_OTHER);
    }
}

<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PermissionType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choices' => [
                'READER' => 'Reader',
                'EDITOR' => 'Editor',
            ],
        ]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
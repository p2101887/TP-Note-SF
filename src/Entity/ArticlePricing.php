<?php

namespace App\Entity;

use App\Repository\ArticlePricingRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ArticlePricingRepository::class)]
class ArticlePricing
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?float $price = null;

    #[ORM\ManyToOne(inversedBy: 'articlePricings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Article $articleId = null;

    #[ORM\ManyToOne(inversedBy: 'articlesPrice')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Store $storeId = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getArticleId(): ?Article
    {
        return $this->articleId;
    }

    public function setArticleId(?Article $articleId): self
    {
        $this->articleId = $articleId;

        return $this;
    }

    public function getStoreId(): ?Store
    {
        return $this->storeId;
    }

    public function setStoreId(?Store $storeId): self
    {
        $this->storeId = $storeId;

        return $this;
    }
}

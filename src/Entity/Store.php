<?php

namespace App\Entity;

use App\Repository\StoreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StoreRepository::class)]
class Store
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'storeId', targetEntity: ArticlePricing::class)]
    private Collection $articlesPrice;

    public function __construct()
    {
        $this->articlesPrice = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, ArticlePricing>
     */
    public function getArticlesPrice(): Collection
    {
        return $this->articlesPrice;
    }

    public function addArticlesPrice(ArticlePricing $articlesPrice): self
    {
        if (!$this->articlesPrice->contains($articlesPrice)) {
            $this->articlesPrice->add($articlesPrice);
            $articlesPrice->setStoreId($this);
        }

        return $this;
    }

    public function removeArticlesPrice(ArticlePricing $articlesPrice): self
    {
        if ($this->articlesPrice->removeElement($articlesPrice)) {
            // set the owning side to null (unless already changed)
            if ($articlesPrice->getStoreId() === $this) {
                $articlesPrice->setStoreId(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\ShoppingListArticlesQuantityRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ShoppingListArticlesQuantityRepository::class)]
class ShoppingListArticlesQuantity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $quantity = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Article $articleId = null;

    #[ORM\ManyToOne(inversedBy: 'articlesQuantity')]
    #[ORM\JoinColumn(nullable: false, onDelete: "CASCADE")]
    private ?ShoppingList $shoppingListId = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Store $store = null;

    #[ORM\Column]
    private ?bool $purshased = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getArticleId(): ?Article
    {
        return $this->articleId;
    }

    public function setArticleId(?Article $articleId): self
    {
        $this->articleId = $articleId;

        return $this;
    }

    public function getShoppingListId(): ?ShoppingList
    {
        return $this->shoppingListId;
    }

    public function setShoppingListId(?ShoppingList $shoppingListId): self
    {
        $this->shoppingListId = $shoppingListId;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    public function isPurshased(): ?bool
    {
        return $this->purshased;
    }

    public function setPurshased(bool $purshased): self
    {
        $this->purshased = $purshased;

        return $this;
    }
}

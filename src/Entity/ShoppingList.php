<?php

namespace App\Entity;

use App\Repository\ShoppingListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ShoppingListRepository::class)]
class ShoppingList
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToMany(mappedBy: 'shoppingListId', targetEntity: ShoppingListArticlesQuantity::class)]
    private Collection $articlesQuantity;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    public function __construct()
    {
        $this->articlesQuantity = new ArrayCollection();
    }

    public function __toString(){
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, ShoppingListArticlesQuantity>
     */
    public function getArticlesQuantity(): Collection
    {
        return $this->articlesQuantity;
    }

    public function addArticlesQuantity(ShoppingListArticlesQuantity $articlesQuantity): self
    {
        if (!$this->articlesQuantity->contains($articlesQuantity)) {
            $this->articlesQuantity->add($articlesQuantity);
            $articlesQuantity->setShoppingListId($this);
        }

        return $this;
    }

    public function removeArticlesQuantity(ShoppingListArticlesQuantity $articlesQuantity): self
    {
        if ($this->articlesQuantity->removeElement($articlesQuantity)) {
            // set the owning side to null (unless already changed)
            if ($articlesQuantity->getShoppingListId() === $this) {
                $articlesQuantity->setShoppingListId(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}

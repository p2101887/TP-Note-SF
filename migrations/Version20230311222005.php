<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230311222005 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE shopping_list_articles_quantity DROP FOREIGN KEY FK_958AE5C6B918ED02');
        $this->addSql('ALTER TABLE shopping_list_articles_quantity ADD CONSTRAINT FK_958AE5C6B918ED02 FOREIGN KEY (shopping_list_id_id) REFERENCES shopping_list (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE shopping_list_articles_quantity DROP FOREIGN KEY FK_958AE5C6B918ED02');
        $this->addSql('ALTER TABLE shopping_list_articles_quantity ADD CONSTRAINT FK_958AE5C6B918ED02 FOREIGN KEY (shopping_list_id_id) REFERENCES shopping_list (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230308133846 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE access_shopping_list (id INT AUTO_INCREMENT NOT NULL, user_id_id INT NOT NULL, shopping_list_id_id INT NOT NULL, permission VARCHAR(255) NOT NULL, INDEX IDX_A5CFC87D9D86650F (user_id_id), INDEX IDX_A5CFC87DB918ED02 (shopping_list_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_pricing (id INT AUTO_INCREMENT NOT NULL, article_id_id INT NOT NULL, store_id_id INT NOT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_823E8848F3EC46 (article_id_id), INDEX IDX_823E88437AC84E (store_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE access_shopping_list ADD CONSTRAINT FK_A5CFC87D9D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE access_shopping_list ADD CONSTRAINT FK_A5CFC87DB918ED02 FOREIGN KEY (shopping_list_id_id) REFERENCES shopping_list (id)');
        $this->addSql('ALTER TABLE article_pricing ADD CONSTRAINT FK_823E8848F3EC46 FOREIGN KEY (article_id_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE article_pricing ADD CONSTRAINT FK_823E88437AC84E FOREIGN KEY (store_id_id) REFERENCES store (id)');
        $this->addSql('ALTER TABLE user_shopping_list DROP FOREIGN KEY FK_D34F090923245BF9');
        $this->addSql('ALTER TABLE user_shopping_list DROP FOREIGN KEY FK_D34F0909A76ED395');
        $this->addSql('ALTER TABLE store_article DROP FOREIGN KEY FK_1B322F817294869C');
        $this->addSql('ALTER TABLE store_article DROP FOREIGN KEY FK_1B322F81B092A811');
        $this->addSql('ALTER TABLE shopping_list_article DROP FOREIGN KEY FK_CFA41AD23245BF9');
        $this->addSql('ALTER TABLE shopping_list_article DROP FOREIGN KEY FK_CFA41AD7294869C');
        $this->addSql('DROP TABLE user_shopping_list');
        $this->addSql('DROP TABLE store_article');
        $this->addSql('DROP TABLE shopping_list_article');
        $this->addSql('ALTER TABLE article CHANGE description description LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_shopping_list (user_id INT NOT NULL, shopping_list_id INT NOT NULL, INDEX IDX_D34F090923245BF9 (shopping_list_id), INDEX IDX_D34F0909A76ED395 (user_id), PRIMARY KEY(user_id, shopping_list_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE store_article (store_id INT NOT NULL, article_id INT NOT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_1B322F817294869C (article_id), INDEX IDX_1B322F81B092A811 (store_id), PRIMARY KEY(store_id, article_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE shopping_list_article (shopping_list_id INT NOT NULL, article_id INT NOT NULL, quantity INT NOT NULL, INDEX IDX_CFA41AD7294869C (article_id), INDEX IDX_CFA41AD23245BF9 (shopping_list_id), PRIMARY KEY(shopping_list_id, article_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE user_shopping_list ADD CONSTRAINT FK_D34F090923245BF9 FOREIGN KEY (shopping_list_id) REFERENCES shopping_list (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_shopping_list ADD CONSTRAINT FK_D34F0909A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE store_article ADD CONSTRAINT FK_1B322F817294869C FOREIGN KEY (article_id) REFERENCES article (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE store_article ADD CONSTRAINT FK_1B322F81B092A811 FOREIGN KEY (store_id) REFERENCES store (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE shopping_list_article ADD CONSTRAINT FK_CFA41AD23245BF9 FOREIGN KEY (shopping_list_id) REFERENCES shopping_list (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE shopping_list_article ADD CONSTRAINT FK_CFA41AD7294869C FOREIGN KEY (article_id) REFERENCES article (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE access_shopping_list DROP FOREIGN KEY FK_A5CFC87D9D86650F');
        $this->addSql('ALTER TABLE access_shopping_list DROP FOREIGN KEY FK_A5CFC87DB918ED02');
        $this->addSql('ALTER TABLE article_pricing DROP FOREIGN KEY FK_823E8848F3EC46');
        $this->addSql('ALTER TABLE article_pricing DROP FOREIGN KEY FK_823E88437AC84E');
        $this->addSql('DROP TABLE access_shopping_list');
        $this->addSql('DROP TABLE article_pricing');
        $this->addSql('ALTER TABLE article CHANGE description description VARCHAR(255) NOT NULL');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230308134919 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE shopping_list_articles_quantity (id INT AUTO_INCREMENT NOT NULL, article_id_id INT NOT NULL, shopping_list_id_id INT NOT NULL, quantity INT NOT NULL, INDEX IDX_958AE5C68F3EC46 (article_id_id), INDEX IDX_958AE5C6B918ED02 (shopping_list_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE shopping_list_articles_quantity ADD CONSTRAINT FK_958AE5C68F3EC46 FOREIGN KEY (article_id_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE shopping_list_articles_quantity ADD CONSTRAINT FK_958AE5C6B918ED02 FOREIGN KEY (shopping_list_id_id) REFERENCES shopping_list (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE shopping_list_articles_quantity DROP FOREIGN KEY FK_958AE5C68F3EC46');
        $this->addSql('ALTER TABLE shopping_list_articles_quantity DROP FOREIGN KEY FK_958AE5C6B918ED02');
        $this->addSql('DROP TABLE shopping_list_articles_quantity');
    }
}

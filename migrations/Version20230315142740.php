<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230315142740 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE shopping_list_articles_quantity ADD store_id INT');
        $this->addSql('ALTER TABLE shopping_list_articles_quantity ADD CONSTRAINT FK_958AE5C6B092A811 FOREIGN KEY (store_id) REFERENCES store (id)');
        $this->addSql('CREATE INDEX IDX_958AE5C6B092A811 ON shopping_list_articles_quantity (store_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE shopping_list_articles_quantity DROP FOREIGN KEY FK_958AE5C6B092A811');
        $this->addSql('DROP INDEX IDX_958AE5C6B092A811 ON shopping_list_articles_quantity');
        $this->addSql('ALTER TABLE shopping_list_articles_quantity DROP store_id');
    }
}

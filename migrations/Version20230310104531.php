<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230310104531 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE access_shopping_list DROP FOREIGN KEY FK_A5CFC87DB918ED02');
        $this->addSql('ALTER TABLE access_shopping_list ADD CONSTRAINT FK_A5CFC87DB918ED02 FOREIGN KEY (shopping_list_id_id) REFERENCES shopping_list (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE access_shopping_list DROP FOREIGN KEY FK_A5CFC87DB918ED02');
        $this->addSql('ALTER TABLE access_shopping_list ADD CONSTRAINT FK_A5CFC87DB918ED02 FOREIGN KEY (shopping_list_id_id) REFERENCES shopping_list (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
    }
}
